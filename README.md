# Autoload PHP
    - O autoload, tem que ser configurado com meu composer.json
    <cod>
        {
   2   │   "autoload": {
   3   │     "psr-4": {
   4   │       "Rodriggo\\MeuApp\\": "src/"
   5   │     }
   6   │   }
   7   │ }
    </cod>

    - tem que seguir a psr-4
    - geralmente usamos o composer para quando vamos instalar algum pace de fora, server como npm ou yarn no node
    - Ele e estruturado como ta ali acima. 
    - ex: Meu projeto e dentro dele tem que ter o arquivo composer.json, um index.php ou main.php aqui e o arquivo principal
    - dentro desse arquivo index.php ou main.php -> teu que fazer o carregamento do autoloado-> require, require_once '../vendor/autolado';
    - e nos aquivos aonde eu quero "exportar" a classe -> eu tenho que usar namespace Rodriggo\MeuApp; Como ta no meu psr-4 no arquivo composer.js
    - e no outro arquivo onde vou usar essa classe tem que ter use Rodriggo\MeuApp\Controller\NomedaClasse.
    - no meu exemplo, nao tem a pasta controller. E os NOME DO ARQUIVOS, TEM QUE SER EM MAISCULO.

<?php

namespace Rodriggo\MeuApp;


class Teste 
{
    private string $name;    
    public function getName():string
    {
        return $this->name;
    }

    public function setName(string $newName):void
    {
        $this->name = $newName;
    }
}

// $teste = new Teste();
// $teste->setName("rodrigo");
// $teste->getName();
// echo "Ola {$teste->getName()}";
